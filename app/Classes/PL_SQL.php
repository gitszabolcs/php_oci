<?php
namespace App\Classes;

class PL_SQL{

	public $conn=null;
	public function __construct()
	{
		//$conn = null;
		$this->conn = oci_connect('php_oci','php_oci','localhost/XE') or die;
		if (!$this->conn) {
    		$e = oci_error();
    		var_dump($e);
		}
	}

	public function test1(){
		$sql = 'select test_package.test1(\'asdasd\')
		from dual';
		$stmt = oci_parse($this->conn,$sql);
		oci_execute($stmt);
    	while ($row = oci_fetch_array($stmt, OCI_ASSOC+OCI_RETURN_NULLS)) {
        		$retval = $row;
		}
		dd($retval);
	}

	function hasRight($user_id, $right_name){
		$sql = 'select HAS_RIGHT('.$user_id.',\''. $right_name .'\') from dual';
		$stmt = oci_parse($this->conn,$sql);
		oci_execute($stmt);
    	while ($row = oci_fetch_array($stmt, OCI_ASSOC+OCI_RETURN_NULLS)) {
        		$retval = $row;
		}
		return $retval['HAS_RIGHT(3,\'PROMOTE_USER\')'];
	}

	public function get_purchase_history($userid){
		$sql="SELECT GET_PURCHASE_HISTORY(".$userid.") as kutya from dual";
		$stmt = oci_parse($this->conn, $sql);
		oci_execute($stmt);
		$ret=[];
		while ($row = oci_fetch_array($stmt, OCI_ASSOC)) {
        		$rc = $row['KUTYA'];
        		oci_execute($rc);
        		while (($rc_row = oci_fetch_array($rc, OCI_ASSOC))) {   
			        $ret[] = $rc_row;
			    }
			    oci_free_statement($rc);
		}
		oci_free_statement($stmt);
		return $ret;
	}

	public function get_products($userid){
		$sql="SELECT GET_PRODUCTS(".$userid.") as kutya2 from dual";
		$stmt = oci_parse($this->conn, $sql);
		oci_execute($stmt);
		$ret=[];
		while ($row = oci_fetch_array($stmt, OCI_ASSOC)) {
        		$rc = $row['KUTYA2'];
        		oci_execute($rc);
        		while (($rc_row = oci_fetch_array($rc, OCI_ASSOC))) {   
			        $ret[] = $rc_row;
			    }
			    oci_free_statement($rc);
		}
		oci_free_statement($stmt);
		return $ret;
	}

	public function vasarlas($user_id, $product_id, $darab){
		$sql="declare 
				myvar FLOAT; 
			begin 
				myvar := DO_PURCHASE(".$user_id.",".$product_id.",".$darab.");
				:r := myvar; 
			end;";
		$stmt = oci_parse($this->conn, $sql);
		oci_bind_by_name($stmt, ':r', $r,100);
		oci_execute($stmt);
		oci_free_statement($stmt);
		oci_close($this->conn);
		return $r;
	}
}