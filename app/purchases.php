<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Purchases extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'purchases';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'product_id', 'user_id', 'number_of_products', 'osszeg'];

}
