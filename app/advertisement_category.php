<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Advertisement_Category extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'advertisement_category';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name'];

}
