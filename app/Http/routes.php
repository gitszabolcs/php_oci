<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'WelcomeController@index');

Route::get('home', 'HomeController@index');
/*Superadmin routes*/
Route::get('superadmin', 'SuperAdminController@index');

Route::get('superadmin/promote','SuperAdminController@promote');

/*Admin routes*/
Route::get('admin', 'AdminController@index');

Route::get('admin/create-product-parent-cat', 'AdminController@createProductParentCat');
Route::get('admin/create-product-cat', 'AdminController@createProductCategory');
Route::get('admin/create-adv-cat', 'AdminController@createAdCategory');

Route::get('admin/create-product', 'AdminController@createProduct');
Route::get('admin/delete-product', 'AdminController@deleteProduct');
Route::get('admin/create-adv', 'AdminController@createAd');


/*user Routes*/
Route::get('user', 'UserController@index');

Route::get('user/information','UserController@informaciok');

Route::post('user/subscribe','UserController@setSubscribe');
Route::get('user/subscribe','UserController@getSubscribe');

Route::get('user/vasarlas','UserController@vasarlas');
Route::post('user/vasarlas','UserController@setVasarlas');


Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
	// 'superadmin' => 'SuperAdminController',
	// 'admin' => 'AdminController',
	// 'user' => 'UserController',
]);
