<?php namespace App\Http\Controllers;

use App\Classes\PL_SQL;

class HomeController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (\Auth::user()->role==="0"){
			return redirect('superadmin');
		}else {
			if (\Auth::user()->role==="1"){
				return redirect('admin');
			}else{
				return redirect('user');
			}
		}
	}

}
