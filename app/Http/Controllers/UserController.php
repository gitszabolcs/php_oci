<?php namespace App\Http\Controllers;

use App\Classes\PL_SQL;
use App\products;
use App\tgyhujikyhuji;
use App\Money;
use App\User;
use App\Purchases;
use App\Subscribers;

class UserController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (\Auth::user()->role==="0"){
			return redirect('superadmin');
		}else{
			if (\Auth::user()->role==="1"){
				return redirect('admin');
			}
		}
		Products::get();
		return view('home');
	}

	public function informaciok(){
		/*$parentCategories = \App\Parent_Category::all();
		$categories = \App\Categories::all();
		$adCategories = \App\Advertisement_category::all();
		$myProducts = \App\Products::where('owner_id','=',\Auth::user()->id)->get();
		return view('admin')->with([
			"parentCategories" 	=> $parentCategories,
			"categories"		=> $categories,
			"adCategories"		=> $adCategories,
			"myProducts" 		=> $myProducts,
			]);*/
		$money=\App\Money::where('USER_ID','=',\Auth::user()->id)->get();
		$pl_sql = new \App\Classes\PL_SQL();
		$vasarlasok=$pl_sql->get_purchase_history(\Auth::user()->id);
		return view('userinfo')->with([
			"money" 	=> $money,
			"vasarlasok"		=> $vasarlasok,
		]);
	}

	public function getSubscribe(){
		$hirdetok=\App\User::where('role','=',1)->get(['id','name']);
		return view('user_subscribe')->with([
			"hirdetok" => $hirdetok
			]);
	}

	public function setSubscribe(){
		$hirdeto_id = \Request::input("uid");
		$ok = \App\Subscribers::create([
			"subscriber_id"	=> \Auth::user()->id,
			"owner_id"	=> $hirdeto_id,
			]);
		return $ok;
	}

	public function vasarlas(){
		$pl_sql = new \App\Classes\PL_SQL();
		$products= $pl_sql->get_products(\Auth::user()->id);
		return view('vasarlas')->with([
			"products" => $products,
			]); 
	}

	public function setVasarlas(){
		$product_id= \Request::input("product_id");
		$darab=\Request::input('darab');
		$pl_sql = new \App\Classes\PL_SQL();
		return $pl_sql->vasarlas(\Auth::user()->id, $product_id, $darab);
	}
}
