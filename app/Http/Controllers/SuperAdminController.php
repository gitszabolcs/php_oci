<?php namespace App\Http\Controllers;

use App\Classes\PL_SQL;
use App\User;


class SuperAdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (\Auth::user()->role!=="0"){
			if (\Auth::user()->role==="1"){
				return redirect('admin');
			}else{
				return redirect('user');
			}
		}
		$users= User::all();
		return view('superadmin')->with(['users'=>$users]);
	}

	public function promote(){
		$uid = \Request::input('uid');
		$role = \Request::input('role');
		if (\Auth::user()->role!=="0"){
			if (\Auth::user()->role==="1"){
				return redirect('admin');
			}else{
				return redirect('user');
			}
		}
		$pl_sql = new \App\Classes\PL_SQL();
		$can_promote_user=$pl_sql->hasRight(\Auth::user()->id, "promote_user");
		if ($can_promote_user){
			User::where('id',$uid)->update(['role' => $role]);
			return redirect('superadmin');
		}
		else{
			return "Te nem promovalhatsz usert!";
		}	
	}
}
