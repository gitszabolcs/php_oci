<?php namespace App\Http\Controllers;

use App\Classes\PL_SQL;
use App\products;
use App\parent_category;
use App\advertisement_category;
use App\advertisements;
use App\categories;

class AdminController extends Controller {

	/*
	|--------------------------------------------------------------------------
	| Home Controller
	|--------------------------------------------------------------------------
	|
	| This controller renders your application's "dashboard" for users that
	| are authenticated. Of course, you are free to change or remove the
	| controller as you wish. It is just here to get your app started!
	|
	*/

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	private function redirectIfNotAdmin(){
		if (\Auth::user()->role!=="1"){
			if (\Auth::user()->role==="0"){
				return redirect('superadmin');
			}else{
				return redirect('user');
			}
		}
	}

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
	public function index()
	{
		$this->redirectIfNotAdmin();
		$parentCategories = \App\Parent_Category::all();
		$categories = \App\Categories::all();
		$adCategories = \App\Advertisement_category::all();
		$myProducts = \App\Products::where('owner_id','=',\Auth::user()->id)->get();
		return view('admin')->with([
			"parentCategories" 	=> $parentCategories,
			"categories"		=> $categories,
			"adCategories"		=> $adCategories,
			"myProducts" 		=> $myProducts,
			]);
	}

	function createProduct(){

		/*$pl_sql = new \App\Classes\PL_SQL();

		dd($pl_sql->hasRight(\Auth::user()->id, "promote_user"));
		*/
		$cat_id = \Request::input("cat_id");
		$name = \Request::input("name");
		$price = \Request::input("price");
		$stock = \Request::input("stock");

		\App\Products::create([
			"name"		=> $name,
			"cat_id"	=> $cat_id,
			"price"		=> $price,
			"stock"		=> $stock,
			"owner_id"	=> \Auth::user()->id
			]);
		return redirect('admin');
	}

	function createProductCategory(){
		$parent_cat_id = \Request::input("parent_cat_id");
		$name = \Request::input("name");

		\App\Categories::create([
				"name"			=> $name,
				"parent_cat_id"	=> $parent_cat_id
			]);

		return redirect('admin');
	}

	function createProductParentCat(){
		$catName = \Request::input("p_cat_name");
		$catToCreate = [
			"name" => $catName
		];
		\App\Parent_Category::create($catToCreate);
		return redirect('admin');
	}

	function createAdCategory(){
		$name = \Request::input("name");

		\App\Advertisement_category::create([
			"name" => $name
			]);
		return redirect('admin');
	}

	function createAd(){
		$name = \Request::input("name");
		$cat_id = \Request::input("cat_id");

		\App\Advertisements::create([
			"name" 		=> $name,
			"cat_id"	=> $cat_id,
			"owner_id"	=> \Auth::user()->id
			]);
		return redirect('admin');
	}

	function deleteProduct(){

		$product_id = \Request::input('product_id');

		\App\Products::where('id',$product_id)->delete();

		return redirect('admin');
	}

}
