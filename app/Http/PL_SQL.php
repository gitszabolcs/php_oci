<?php
namespace App\Classes;
// This is an example connection to the oracle database using the php_oci driver
		/*$conn = oci_connect('php_oci','php_oci','localhost/XE') or die;

if (!$conn) {
    $e = oci_error();
    trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
}
$sql = 'select test_package.test1(\'asdasd\')
		from dual';

$stmt = oci_parse($conn,$sql);



// Assign a value to the input 
$valami="asdasd";
$retval="";
oci_execute($stmt);
while ($row = oci_fetch_array($stmt, OCI_ASSOC+OCI_RETURN_NULLS)) {
    foreach ($row as $item) {
        $retval = $retval." ".$item;
    }
}*/

class PL_SQL{

	public function __construct()
	{
		$conn = oci_connect('php_oci','php_oci','localhost/XE') or die;
		if (!$conn) {
    		$e = oci_error();
    		trigger_error(htmlentities($e['message'], ENT_QUOTES), E_USER_ERROR);
		}
	}

	public function test1(){
		$sql = 'select test_package.test1(\'asdasd\')
		from dual';
		$stmt = oci_parse($conn,$sql);
		oci_execute($stmt);
    	while ($row = oci_fetch_array($stmt, OCI_ASSOC+OCI_RETURN_NULLS)) {
        		$retval[] = $row;
		}
		return $retval;
	}

}