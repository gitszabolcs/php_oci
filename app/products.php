<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model{

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'pruducts';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['name', 'owner_id', 'cat_id', 'price', 'stock'];

}
