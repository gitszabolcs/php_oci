@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					<nav class="navbar navbar-default">
					  <div class="container-fluid">
					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul class="nav navbar-nav">
					        <li><a href="/user/information">Informaciok</a></li>
					        <li><a href="/user/subscribe">Feliratkozas</a></li>
					        <li><a href="/user/vasarlas">Vasarlas</a></li>
					        <li><a href="/user/advertisements">Reklamok</a></li>
					      </ul>
					    </div><!-- /.navbar-collapse -->
					  </div><!-- /.container-fluid -->
					</nav>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
