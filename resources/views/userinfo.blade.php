@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					<nav class="navbar navbar-default">
					  <div class="container-fluid">
					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul class="nav navbar-nav">
					        <li class="active"><a href="/user/information">Informaciok <span class="sr-only">(current)</span></a></li>
					        <li><a href="/user/subscribe">Feliratkozas</a></li>
					        <li><a href="/user/vasarlas">Vasarlas</a></li>
					        <li><a href="/user/advertisements">Reklamok</a></li>
					      </ul>
					    </div><!-- /.navbar-collapse -->
					  </div><!-- /.container-fluid -->
					</nav>
					<h3>Onnek jelenleg {{ empty($money)? 0 : ''}}
						@foreach( $money as $penz)
							{{$penz['osszeg']}} {{$penz['valuta']}}
						@endforeach penze van!</h3>
					<h2>Vasarlasi elozmenyek</h2>
					<table class="table table-bordered table-hover">
						<thead>
							<th>Termek</th>
							<th>Darab</th>
							<th>Ossz</th>
						</thead>
						<tbody>
							@foreach($vasarlasok as $vasarlas)
							<tr>
								<td>{{$vasarlas['NAME']}}</td>
								<td>{{$vasarlas['NUMBER_OF_PRODUCTS']}}</td>
								<td>{{isset($vasarlas['OSSZEG']) ? $vasarlas['OSSZEG'] : 'Ingyer volt'}}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
