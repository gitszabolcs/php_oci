@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					<nav class="navbar navbar-default">
					  <div class="container-fluid">
					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul class="nav navbar-nav">
					        <li><a href="/user/information">Informaciok </a></li>
					        <li class="active"><a href="/user/subscribe">Feliratkozas <span class="sr-only">(current)</span></a></li>
					        <li><a href="/user/vasarlas">Vasarlas</a></li>
					        <li><a href="/user/advertisements">Reklamok</a></li>
					      </ul>
					    </div><!-- /.navbar-collapse -->
					  </div><!-- /.container-fluid -->
					</nav>
					<div class='form-inline'>
						<div class="input-group">
							<span class="input-group-addon" id="sizing-addon2">Valassza ki a kivant hirdetot</span>
							<select class="form-control" id="subscribe">
								@foreach($hirdetok as $hirdeto)
									<option value="{{$hirdeto['id']}}">{{$hirdeto['name']}}</option>
								@endforeach
							</select>
						</div>
						<input type="hidden" id="_token" value="<?php echo csrf_token(); ?>">
						<button class="btn btn-primary" id="btn_feliratkozas">Feliratkozas</button>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
	$(document).ready(function(){
		$("#btn_feliratkozas").click(function(){
			var kivalasztott=$("#subscribe").val();
			var laravelToken=$("#_token").val();
			console.log(kivalasztott);
			$.ajax({
		            type: 'post',
		            url: '/user/subscribe',
		            data : {"uid": kivalasztott, "_token": laravelToken},                
		            success: function(data, status){
		                alert('Sikeres feliratkozas!');
		                console.log(data);
		            },
		            error: function(qXHR, textStatus, error) {
		                alert("Ohh my Gosh something went wrong!");
		            },
		    });
		});
	});
	</script>
</div>
@endsection
