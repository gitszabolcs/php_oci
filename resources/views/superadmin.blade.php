@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					You are logged in!
				</div>
				<div class="container row-fluid">
					<h3>User jogok megadasa Superadmin alltal</h3>
					<form method="get" action="/superadmin/promote" class="form-inline">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<select name="uid" class="form-control">
							@foreach($users as $user)
							<option value={{$user['id']}}>{{$user['email']}}</option>
							@endforeach
						</select>
						<select name="role" class="form-control">
							<option value="0">Superadmin</option>
							<option value="1">Admin</option>
							<option value="2">User</option>
						</select>
						<input type="submit" value="Jogosultsag megadasa" class="btn btn-primary" />
					</form>
				</div>
				<!--<div class="container row-fluid">
					<h3>User jogok megadasa Superadmin alltal</h3>
					<form method="get" action="/superadmin/promote">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<select name="uid">
							@foreach($users as $user)
							<option value={{$user['id']}}>{{$user['email']}}</option>
							@endforeach
						</select>
						<select name="role">
							<option value="0">Superadmin</option>
							<option value="1">Admin</option>
							<option value="2">User</option>
						</select>
						<input type="submit" value="Jogosultsag megadasa" />
					</form>
				</div>-->
			</div>

		</div>
	</div>
</div>
@endsection
