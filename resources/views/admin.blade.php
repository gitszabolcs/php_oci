@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					You are logged in!
				</div>
				<div class="container row-fluid">
					<h3>Uj termek fokategoria letrehozasa</h3>
					<form method="get" action="/admin/create-product-parent-cat" class="form-inline">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<input type="text" name="p_cat_name"  class="form-control" placeholder="Fokategoria neve">
						<input type="submit" value="Letrehozas"  class="btn btn-info"/>
					</form>
				</div>
				<div class="container row-fluid">
					<h3>Uj termek kategoria letrehozasa</h3>
					<form method="get" action="/admin/create-product-cat" class="form-inline">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<select name="parent_cat_id" class="form-control">
							
							@foreach($parentCategories as $pCat)
								<option value="{{$pCat['id']}}">{{$pCat['name']}}</option>
							@endforeach

						</select>
						<input type="text" name="name" class="form-control" placeholder="Alkategoria neve">
						<input type="submit" value="Letrehozas"  class="btn btn-info"/>
					</form>
				</div>

				<div class="container row-fluid">
					<h3>Uj termek letrehozasa</h3>
					<form method="get" action="/admin/create-product" class="form-inline">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<select name="cat_id" class="form-control">
							
							@foreach($categories as $cat)
								<option value="{{$cat['id']}}">{{$cat['name']}}</option>
							@endforeach

						</select>
						<input type="text" name="name" class="form-control" placeholder="Termek neve">
						<input type="text" name="price" class="form-control" placeholder="Egysegar">
						<input type="text" name="stock" class="form-control" placeholder="Mennyiseg raktaron">
						<input type="submit" value="Letrehozas"  class="btn btn-info"/>
					</form>
				</div>
				<hr/>
				<div class="container row-fluid">
					<h3>Uj reklam kategoria letrehozasa</h3>
					<form method="get" action="/admin/create-adv-cat" class="form-inline">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<input type="text" name="name"  class="form-control" placeholder="Reklamkategoria neve">
						<input type="submit" value="Letrehozas"  class="btn btn-info"/>
					</form>
				</div>
				<div class="container row-fluid">
					<h3>Uj reklam letrehozasa</h3>
					<form method="get" action="/admin/create-adv" class="form-inline">
						<input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
						<select name="cat_id" class="form-control">

						@foreach($adCategories as $cat)
							<option value="{{$cat['id']}}">{{$cat['name']}}</option>
						@endforeach

						</select>
						<input type="text" name="name" class="form-control" placeholder="Reklam neve">
						<input type="submit" value="Letrehozas"  class="btn btn-info"/>
					</form>
				</div>
				<hr/>
				<div class="container">
				<h3>Termekek</h3>
					<div class='row'>
					<div class="col-md-9">
					<table class="table table-hover table-bordered">
						<thead>
							<th>Kategodia ID</th>
							<th>Nev</th>
							<th>Ar</th>
							<th>Stock</th>
							<th>Torles</th>
						</thead>
						<tbody>
							@foreach($myProducts as $product)
								<tr>
									<td>{{$product['cat_id']}}</td>
									<td>{{$product['name']}}</td> 	
									<td>{{$product['price']}}</td>
									<td>{{$product['stock']}}</td>
									<td> <a href="/admin/delete-product?_token={{{ csrf_token() }}}&product_id={{$product['id']}}">Torol</a></td>
								</tr>
							@endforeach
						</tbody>
					</table>
					</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
@endsection
