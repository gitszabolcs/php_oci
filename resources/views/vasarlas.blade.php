@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Home</div>

				<div class="panel-body">
					<nav class="navbar navbar-default">
					  <div class="container-fluid">
					    <!-- Collect the nav links, forms, and other content for toggling -->
					    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
					      <ul class="nav navbar-nav">
					        <li><a href="/user/information">Informaciok</a></li>
					        <li><a href="/user/subscribe">Feliratkozas</a></li>
					        <li><a href="/user/vasarlas">Vasarlas</a></li>
					        <li><a href="/user/advertisements">Reklamok</a></li>
					      </ul>
					    </div><!-- /.navbar-collapse -->
					  </div><!-- /.container-fluid -->
					</nav>
					<table class="table table-bordered table-hover">
						<thead>
							<th>Elado</th>
							<th>Kategoria</th>
							<th>Termeknev</th>
							<th>Ar</th>
							<th>Raktaron</th>
							<th>Megvesz</th>
						</thead>
						<tbody>
							@foreach($products as $product)
							<tr>
								<td>{{$product['ELADO']}}</td>
								<td>{{$product['KATEGORIA']}}</td>
								<td>{{$product['NAME']}}</td>
								<td>{{$product['PRICE']}}</td>
								<td>{{$product['STOCK']}}</td>
								<td>
									<div class="form-inline">
										<input type="number" class="form-control darab">
										<input type="hidden" id="_token" value="<?php echo csrf_token(); ?>">
										<button class="megvesz btn btn-primary" data-id="{{$product['ID']}}">Megveszem</button>
									</div>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function(){
		$(".megvesz").click(function(){
			var product_id=$(this).data("id");
			var darab=$(this).parent().find('.darab').val();
			var laravelToken=$("#_token").val();
			if( darab.length===0 || (Number(darab)!==darab && darab%1!==0) ){
				alert('Add meg a darabszamot!');
			}else{
				$.ajax({
			            type: 'post',
			            url: '/user/vasarlas',
			            data : {"product_id": product_id, "darab": darab, "_token": laravelToken},                
			            success: function(data, status){
			            	if (data==-1){
			            		alert('Nincs eleg a raktaron!');
			            	}else{
			            		alert('Sikeres vasarlas');
			            	}
			                console.log(data);
			            },
			            error: function(qXHR, textStatus, error) {
			                alert("Ohh my Gosh something went wrong!"+textStatus);
			            },
			    });
			}
		});
	});
	</script
</div>
@endsection
